from django.test import TestCase
from django.urls import resolve
from .views import *


class UnitTest(TestCase):
    indexResponse = None

    def setUp(self):
        self.indexResponse = self.client.get('/')
        self.loginResponse = self.client.get('/login/')
        self.registerResponse = self.client.get('/register')

    def testIndexPageIsExist(self):
        self.assertEqual(self.indexResponse.status_code, 200)

    def testIndexPageUsesIndexHTML(self):
        self.assertTemplateUsed(self.indexResponse, 'index.html')

    def testIndexPageUsingIndexFunc(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def testIndexPageHasHeader(self):
        self.assertContains(self.indexResponse,
                            '<h1>You are not logged in.</h1>')

    def testIndexPageHasLoginButton(self):
        self.assertContains(self.indexResponse, 'href="/login/"')

    def testLoginPageIsExist(self):
        self.assertEqual(self.loginResponse.status_code, 200)

    def testLoginPageUsesLoginHTML(self):
        self.assertTemplateUsed(self.loginResponse, 'registration/login.html')

    def testLoginPageHasUsernameField(self):
        self.assertContains(self.loginResponse,
                            '<input type="text" name="username"')

    def testLoginPageHasPasswordField(self):
        self.assertContains(self.loginResponse,
                            '<input type="password" name="password"')

    def testLoginPageHasSubmitButton(self):
        self.assertContains(self.loginResponse,
                            '<input class="btn btn-primary" type="submit"')

    def testRegisterPageIsExist(self):
        self.assertEqual(self.registerResponse.status_code, 200)

    def testRegisterPageUsesRegisterHTML(self):
        self.assertTemplateUsed(self.registerResponse, 'registration/register.html')

    def testRegisterPageUsingIndexFunc(self):
        found = resolve('/register')
        self.assertEqual(found.func, register)

    def testRegisterPageHasUsernameField(self):
        self.assertContains(self.registerResponse,
                            '<input type="text" name="username"')

    def testRegisterPageHasPasswordField(self):
        self.assertContains(self.registerResponse,
                            '<input type="password" name="password1"')

    def testRegisterPageHasSubmitButton(self):
        self.assertContains(self.registerResponse,
                            '<input class="btn btn-primary" type="submit"')
